r-cran-dimred (0.2.6-4) unstable; urgency=medium

  * Team upload.
  * Fix typo in autopkgtest run script.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 06 Aug 2024 12:33:44 +0200

r-cran-dimred (0.2.6-3) unstable; urgency=medium

  * Team upload.
  * autopkgtests: ignore test_NMMR on riscv64 as well
  * Standards-Version: 4.6.2 (routine-update)
  * d/control: put unmmanaged build dependency (architecture-is-64-bit)
    last, so that dh-update-R doesn't ignore the R build dependencies.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 04 Aug 2024 10:27:42 +0200

r-cran-dimred (0.2.6-2) unstable; urgency=medium

  * Team upload.
  * d/control: stop building for 32-bit architectures as upstream doesn't
    support them. Closes: #1074529

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 12 Jul 2024 12:02:03 +0200

r-cran-dimred (0.2.6-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jul 2022 09:08:59 +0200

r-cran-dimred (0.2.5-3) unstable; urgency=medium

  * Team upload
  * Also ignore test_NNMF on s390x

 -- Graham Inggs <ginggs@debian.org>  Sun, 22 May 2022 11:34:29 +0000

r-cran-dimred (0.2.5-2) unstable; urgency=medium

  * Team Upload.
  * Ignore test_NMMR on a arm and ppc

 -- Nilesh Patra <nilesh@debian.org>  Fri, 18 Mar 2022 22:05:29 +0530

r-cran-dimred (0.2.5-1) unstable; urgency=medium

  * New upstream version
  * Add Suggests from DESCRIPTION to Test-Depends
  * Exclude tests with missing dependencies

 -- Andreas Tille <tille@debian.org>  Wed, 16 Mar 2022 13:42:20 +0100

r-cran-dimred (0.2.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 20 Dec 2021 14:17:50 +0100

r-cran-dimred (0.2.3-2) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Wed, 02 Sep 2020 16:49:48 +0200

r-cran-dimred (0.2.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Exclude sufficient amount of autopkgtest to let it pass

 -- Andreas Tille <tille@debian.org>  Thu, 18 Jul 2019 15:10:57 +0200

r-cran-dimred (0.2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Sun, 18 Nov 2018 18:11:17 +0100

r-cran-dimred (0.1.0-2) unstable; urgency=medium

  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Thu, 21 Jun 2018 06:31:51 +0200

r-cran-dimred (0.1.0-1) unstable; urgency=medium

  * Initial release (closes: #883054)

 -- Andreas Tille <tille@debian.org>  Wed, 29 Nov 2017 09:20:46 +0100

#!/bin/sh -e

pkgname=dimRed
debname=r-cran-dimred

if [ "$AUTOPKGTEST_TMP" = "" ] ; then
    AUTOPKGTEST_TMP=`mktemp -d /tmp/${debname}-test.XXXXXX`
#    trap "rm -rf $AUTOPKGTEST_TMP" 0 INT QUIT ABRT PIPE TERM
fi
set -x
cd $AUTOPKGTEST_TMP
cp -a /usr/share/doc/$debname/tests/* $AUTOPKGTEST_TMP
gunzip -r *

# UMAP is not available - so ignore the test
find . \( -name test_umap.R -o \
          -name test_isomap.R -o \
          -name test_diffmap.R -o \
          -name test_HLLE.R \) -delete
find . -name test_quality.R -exec \
   sed -i -e '/test_that("AUC_lnK_R_NX"/,$d' \
          -e '/suppressWarnings(/,/lapply(resQual, function(x) expect_true/d' \
          -e '/tmp <- sapply(1:4, function(x) quality(resPCA, "Q_local", ndim = x))/,/expect_equal(rank(tmp), 1:4)/d' \
      \{\} \;
find . -name test_misc.R -exec \
   sed -i -e '/irisRes <- embed(irisData, "tSNE")/,/expect_error(getRotationMatrix/d' \
      \{\} \;
find . -name test_dimRedResult.R -exec \
   sed -i -e '/emb2 <- embed(dat, "tSNE")/,/expect_error(inverse(emb2, dat))/d' \
      \{\} \;
find . -name test_all.R -exec \
   sed -i -e '/for (e in embed_methods) {/,/^    }/d' \
          -e '/expect(storage.mode(quality_results) == "double"/,/storage should be "double"/d' \
      \{\} \;
find . -name test_autoencoder.R -exec \
   sed -i -e '/test_that("Check if tensorflow is installed correctly."/,$d' \
      \{\} \;
find . -name test_NNMF.R -exec \
   sed -i -e '/test_that("Full_rank", /,$d' \
      \{\} \;
find . -name test_quality.R -exec \
   sed -i -e '/test_that("quality", /,/^})$/d' \
          -e '/test_that("Q_local ndim", /,/^})$/d' \
      \{\} \;
find . -name test_diffusion_maps.R -delete
find . -name test_high_level_functions.R -delete # several not existing packages will be checked here
# FIXME this test references package umap-learn.  Since this is not in testing currently, skip this test
find . -name test_UMAP.R -delete

hostarch=$(dpkg-architecture -qDEB_HOST_ARCH)
if [ "$hostarch" = "armhf" -o "$hostarch" = "arm64" -o "$hostarch" = "ppc64el" -o "$hostarch" = "s390x" -o "$hostarch" = "riscv64" ] ; then
    find . -name test_NNMF.R -delete
fi

for testfile in *.R; do
    echo "BEGIN TEST $testfile"
    LC_ALL=C R --no-save < $testfile
done

